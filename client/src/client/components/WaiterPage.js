import React, { Component } from "react";
import { connect } from "react-redux";
import { addWaiter, getWaiter } from "../actions/waiters.action.js";
import { setPageTitle } from '../actions/common.action';
import yourTurnSound from './sounds/your-turn-sound.mp3';

var audio = new Audio(yourTurnSound);

class ServicesPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            timerGetWaiter: '',
            userAlreadyInvited: false
        }
    }

    // addWaiter = (serviceId) => {
    //     let formData = new FormData();    //formdata object
    //     formData.append('service_id', serviceId);   //append the values with key, value pair
    //     this.props.addWaiter(formData);
    //     this.props.history.push('/diplomna/waiter')
    // }

    componentDidMount() {
        this.props.setPageTitle('Готово, изчакайте своя ред!');

        const self = this;
        let timerGetWaiter = setInterval(function () {
            self.props.getWaiter(self.props.waiter.id);
            if (self.state.userAlreadyInvited == false && self.props.waiter.status) {
                if (self.props.waiter.status == 'in_progress') {
                    audio.play();
                    setTimeout(() => {
                        audio.play();
                    }, 2000);
                    setTimeout(() => {
                        audio.play();
                    }, 4000);
                    setTimeout(() => {
                        audio.play();
                    }, 6000);
                    self.setState({ userAlreadyInvited: true })
                }
            }

        }, 5000);
        this.setState({ timerGetWaiter });
    }

    componentWillUnmount() {
        clearInterval(this.state.timerGetWaiter);
    }

    render() {
        const waiter = this.props.waiter;

        const selectedService = this.props.services.find(s => s.id == waiter.service_id)

        return (
            <React.Fragment>
                {waiter.status == 'waiting' ?
                    <div style={{ margin: '0 auto', textAlign: 'center', padding: 15 }}>
                        <h2>Вашият номер е: {waiter.number}</h2>

                        <hr />

                        <h3>Избрана услуга: {selectedService && selectedService.name ? selectedService.name : ''}</h3>
                        <h3>Брой на чакащите: {waiter.waitersSameService}</h3>

                        <hr />

                        <h3>Време на пристигане: {waiter.created_at}</h3>
                        <h4>Статус: {waiter.status}</h4>

                        <hr />

                        <p><i>Номерът на билета не отговаря на реда на повикване!</i></p>
                    </div>
                    :
                    waiter.status == 'in_progress' ?
                        <div style={{ margin: '0 auto', textAlign: 'center', padding: 15 }}>
                            <h2>Заповядайте на гише: {waiter.table_num}</h2>
                            <h3>Вас ще ви обслужи: {this.props.users.find(u => u.id == waiter.employee_id).name}</h3>
                            <hr />
                            <i>Благодарим Ви, че сте наш клиент!</i>
                        </div>
                        :
                        waiter.status == 'completed' ?
                            <div style={{ margin: '0 auto', textAlign: 'center', padding: 15 }}>
                                <h2>Благодарим за посещението!</h2>
                                <h4>Вас ви обслужи: {this.props.users.find(u => u.id == waiter.employee_id).name}</h4>

                                <hr />

                                <i>Бихме се радвали, ако ни оцените!</i>
                            </div>
                            :
                            waiter.status == 'canceled' ?
                                <div style={{ margin: '0 auto', textAlign: 'center', padding: 15 }}>
                                    <h2>За съжаление не можем да ви обслужим в момента.</h2>
                                    <h4>Моля потърсете повече информация от наш служител.</h4>

                                    <hr />

                                    <i>Съжаляваме за неудобството :/</i>
                                </div>
                                :
                                null
                }

            </React.Fragment>
        );
    }
}

const mapStateToProops = (state, ownProps) => {
    return {
        waiter: state.waiter,
        services: state.services.services,
        common: state.common,
        users: state.users.users
    };
};

export default connect(mapStateToProops, { getWaiter, addWaiter, setPageTitle })(ServicesPage);
