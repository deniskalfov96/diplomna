import { GET_QRCODES } from '../actions/types';

const initialState = {
    qrcodes: [],
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_QRCODES:
            return {
                ...state,
                qrcodes: action.payload
            }
        default:
            return state;
    }
}