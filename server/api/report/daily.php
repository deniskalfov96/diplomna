<?php
header("Access-Control-Request-Method: GET");

include_once ROOT.'objects/Report.php';
include_once ROOT.'objects/Deposit.php';

define('INCREASE_PERCENT', 0.01);

$data = $_GET;
$data['reportDate'] = htmlspecialchars(strip_tags($data['reportDate']));

$deposit = new Deposit($db);
$report = new Report($db);

// Get Deposit sum without increase and Deposit count
$depositSumWithoutIncrease = 0;
$depositCount = 0;

$stmt = $deposit->getSumAndCountOfDepositsByDate($data['reportDate']);
$num = $stmt->rowCount();
if ($num > 0) {
    $row = $stmt->fetch(PDO::FETCH_ASSOC);
    $depositSumWithoutIncrease = $row['deposit_sum'] ? $row['deposit_sum'] : 0;
    $depositCount = $row['deposit_count'] ? $row['deposit_count'] : 0;
}

// Add increase to deposit
$increaseAmountStatus = $deposit->increaseAmount(INCREASE_PERCENT, $data['reportDate']);

if ($increaseAmountStatus === false) {
    // set response code - Not Found
    http_response_code(404);

    echo json_encode(
        array("message" => "Could not increase amounts")
    );
    die;
}

// Calculate Deposit sum with increase
$depositSumWithIncrease = $depositSumWithoutIncrease + (INCREASE_PERCENT * $depositSumWithoutIncrease / 100);

// Prepare report values
$reportValues['increase_amount'] = round($depositSumWithIncrease - $depositSumWithoutIncrease, 5);
$reportValues['increase_amount_usd'] = calculateBGNtoUSD($reportValues['increase_amount']);
$reportValues['count_deposits'] = $depositCount;
$reportValues['date_created'] = $data['reportDate'];

// Insert deposit report values to the report table
$stmt = $report->insertReport($reportValues);

$report_arr[] = $reportValues;

// set response code - 200 OK
http_response_code(200);

echo json_encode($report_arr);

function calculateBGNtoUSD($bgnValue) {
    $currencies = file_get_contents("http://data.fixer.io/api/latest?access_key=0d52da9f2090212bec148d7cd9d858b1");
    $currencies = json_decode($currencies, true);
    $bgnRate = 0;
    $usdRate = 0;

    if ($currencies['success'] === true) {
        $bgnRate = $currencies['rates']['BGN'];
        $usdRate = $currencies['rates']['USD'];

        return round(($bgnValue / $bgnRate) * $usdRate, 5);
    }

    return -1; //Cant convert
}