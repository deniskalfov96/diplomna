import React, { Component } from "react";
import { withStyles } from '@material-ui/styles';
import { connect } from "react-redux";
import Router from "./Router";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import { Link } from "react-router-dom";

const styles = theme => ({
	root: {
		flexGrow: 1,
	},
	menuButton: {
		marginRight: 8,
	},
	title: {
		flexGrow: 1,
	},
});

class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			auth: true,
			anchorEl: null,
		}
	}


	handleMenu = (event) => {
		this.setState({ anchorEl: event.currentTarget });
	};

	handleClose = () => {
		this.setState({ anchorEl: null });
	};

	render() {
		const { classes } = this.props;
		let open = Boolean(this.state.anchorEl)

		return (
			<React.Fragment>
				{/* <AppNavbar /> */}
				<div className="page-container">

					<AppBar position="static">
						<Toolbar>
							<Typography variant="h6" 
							className={classes.title}
							>
								{this.props.pagetitle}
							</Typography>
							{this.state.auth && (
								<div>
									<IconButton
										aria-label="account of current user"
										aria-controls="menu-appbar"
										aria-haspopup="true"
										onClick={this.handleMenu}
										color="inherit"
									>
										<MoreVertIcon />
									</IconButton>
									<Menu
										id="menu-appbar"
										anchorEl={this.state.anchorEl}
										anchorOrigin={{
											vertical: 'top',
											horizontal: 'right',
										}}
										keepMounted
										transformOrigin={{
											vertical: 'top',
											horizontal: 'right',
										}}
										open={open}
										onClose={this.handleClose}
									>
										<MenuItem onClick={this.handleClose}>Докладвайте за бъг</MenuItem>
										<MenuItem onClick={this.handleClose}><Link to="/diplomna/employee/login" style={{ color: 'inherit', textDecoration: 'none' }}>Система за Служители</Link></MenuItem>
										<MenuItem onClick={this.handleClose}><Link to="/diplomna/admin/login" style={{ color: 'inherit', textDecoration: 'none' }}>Система за Администратори</Link></MenuItem>
									</Menu>
								</div>
							)}
						</Toolbar>
					</AppBar>

					<Router />
				</div>
			</React.Fragment>
		);
	}
}

const mapStateToProps = (state, ownProps) => {
	console.log(`state`, state)
	return {
		pagetitle: state.common.pagetitle,
	};
};
export default (withStyles(styles)(connect(mapStateToProps, {})(App)))
