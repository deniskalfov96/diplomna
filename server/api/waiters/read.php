<?php
header("Access-Control-Request-Method: GET");

include_once ROOT.'objects/Waiter.php';
include_once ROOT.'objects/UserService.php';

$waiter = new Waiter($db);
$userService = new UserService($db);
$data = $_GET;

if ($data['waiterId']) {
    $stmt = $waiter->getAllByWaiterId($data['waiterId']);
} else if ($data['employeeId']) {
    $stmt = $userService->getServiceIdsByEmployeeId($data['employeeId']);
    $num = $stmt->rowCount();
    if ($num > 0) {
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $obj_arr[] = $row;
        }
        $stmt = $waiter->getAllByServiceIds($obj_arr);
    }
} else {
    $stmt = $waiter->getAll();
}

$num = $stmt->rowCount();

if ($num > 0) {
    $obj_arr = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
        $row["waitersSameService"] = ($waiter->getWaitingWaitersForSameService($row['service_id']))->rowCount();
        $obj_arr[] = $row;
    }

    // set response code - 200 OK
    http_response_code(200);

    echo json_encode($obj_arr);
} else {
    // set response code - Not Found
    http_response_code(404);

    echo json_encode(
        array("message" => "No records")
    );
}
