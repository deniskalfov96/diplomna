import axios from 'axios';
import { GET_ALL_WAITERS, GET_ALL_WAITERS_FOR_EMPLOYEE } from './types';
import { API_URL } from '../../constants'

export const getAllWaiters = () => dispatch => {
    axios
        .get(API_URL + '?page=waiters/read')
        .then(res => {
            dispatch({
                type: GET_ALL_WAITERS,
                payload: res.data
            })
        }
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}

export const getAllWaitersForCurrentEmployee = (employee) => dispatch => {
    employee = JSON.parse(employee);

    axios
        .get(API_URL + '?page=waiters/read&employeeId=' + employee.id)
        .then(res => {
            dispatch({
                type: GET_ALL_WAITERS_FOR_EMPLOYEE,
                payload: res.data
            })
        }
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}


export const updateWaiter = (data) => dispatch => {
    axios
        .post(API_URL + '?page=waiters/edit', data)
        .then(res => {
            console.log(`res`, res)
            dispatch(getAllWaitersForCurrentEmployee(localStorage.getItem('login_employee')));
        }).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}

// export const updateWaiters = () => dispatch => {

// }