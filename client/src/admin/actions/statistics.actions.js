import axios from 'axios';
import { GET_STATISTICS } from './types';
import { API_URL } from '../../constants'

export const getStatistics = () => dispatch => {
    // console.log('getting info')
    axios
        .get(API_URL + '?page=statistics/read')
        .then(res =>
            // console.log("services ", res.data),
            dispatch({
                type: GET_STATISTICS,
                payload: res.data
            })
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}
