<?php
include_once ROOT.'objects/Waiter.php';

$waiter = new Waiter($db);
$data = (object)$_POST;

$waiter->number = $waiter->getUniqueDailyNumber();
$waiter->service_id = $data->service_id;
$waiter->table_num= $data->table_num;
$waiter->status = "waiting";
$waiter->created_at = date("Y-m-d H:i:s");

$createdWaiter = $waiter->create();

if ($createdWaiter) {
    http_response_code(200);
    echo
        json_encode(
            array(
                "id" => $createdWaiter,
                "number" => $waiter->number,
                "status" => $waiter->status,
                "employee_id" => $waiter->employee_id,
                "created_at" => $waiter->created_at,
                "service_id" => $waiter->service_id,
                "table_num" => $waiter->table_num,
                "waitersSameService" => ($waiter->getWaitingWaitersForSameService($waiter->service_id))->rowCount(),
            )
        );
} else {
    http_response_code(503);
    echo json_encode(array("error" => "Error occurred while adding record"));
}

?>