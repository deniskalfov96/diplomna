export const GET_SERVICES = 'GET_SERVICES';
export const ADD_WAITER = 'ADD_WAITER';
export const GET_WAITER = 'GET_WAITER';
export const GET_QRCODES = 'GET_QRCODES';
export const GET_PLACES = 'GET_PLACES';
export const SAVE_SCANNED_PLACE = 'SAVE_SCANNED_PLACE';
export const SET_PAGE_TITLE = 'SET_PAGE_TITLE';
export const GET_USERS = 'GET_USERS';