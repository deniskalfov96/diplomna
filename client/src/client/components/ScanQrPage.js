import React, { Component } from "react";
import { connect } from "react-redux";
// import { signInAction } from "../../actions/authActions";
import QrReader from "react-qr-reader";
// import Utils from "../utils/Utils";
import { getServices } from '../actions/services.action';
import { saveScannedPlace, setPageTitle } from '../actions/common.action';

// import { checkValidQR } from './../actions/scannedQrActions'

class ScanQrPage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			delay: 300,
			scannedMessage: null
		};
	}

	componentDidMount() {
		this.props.setPageTitle('Сканирайте QR код');
		this.handleScan();
	}

	handleScan = (scannedQr) => {
		if (scannedQr) {
			const validQr = (this.props.qrcodes.find(q => q.token == scannedQr && q.active == 1));
			if (validQr) {
				this.props.getServices(validQr.place_id);
				this.props.saveScannedPlace({
					placeId: validQr.place_id,
					placeName: this.props.places.find(p => p.id == validQr.place_id).name
				})
				this.props.history.push('/diplomna/services'); // Redirect
			} else {
				this.setState({
					scannedMessage: 'Неправилен QR Код'
				})
			}
		} else {
			const visited_place = this.props.common && this.props.common.scanned_place;
			if (visited_place) {
				this.setState({
					scannedMessage:
						`Избраният обект е: ${visited_place.placeName} 
					Натиснете за да продължите или сканирайте отново, 
					за да заредите услугите от обекта в който се намирате.`
				})
			}
		}
	}

	handleError(err) {
		console.error(err);
	}

	render() {
		return (
			<div>
				{this.state.scannedMessage ?
					<p onClick={() => this.props.history.push('/diplomna/services')}>{this.state.scannedMessage}</p>
					:
					<p style={{ textAlign: 'center' }}>В момента не е избран обект. Сканирайте за да се заредят услугите.</p>
				}
				<QrReader
					delay={this.state.delay}
					onError={this.handleError}
					onScan={this.handleScan}
					style={{ width: "100%", maxWidth: 500, margin: '0 auto' }}
				/>
			</div>
		);
	}
}



const mapStateToProops = state => {

	return {
		qrcodes: state.qrcodes.qrcodes,
		places: state.places.places,
		common: state.common,
	};
};

export default connect(mapStateToProops, { getServices, saveScannedPlace, setPageTitle })(ScanQrPage);