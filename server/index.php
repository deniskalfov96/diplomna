<?php
// required header
header("Access-Control-Allow-Origin: *");

date_default_timezone_set('Europe/Sofia');
define('ROOT', '/home/tablbglm/denis.tabl.bg/diplomna_api/api/');

include_once(ROOT. 'config/database.php');

$database = new Database();
$db = $database->getConnection();

$page = !empty($_GET['page']) ? $_GET['page'] : "/";

$data = json_decode(file_get_contents("php://input"));
if ($data) {
    $_POST = array_merge($_POST, (array)$data);
}

$apiFileRoute = ROOT . $page . ".php";

// Check if file exists
if (file_exists($apiFileRoute)) {
    require($apiFileRoute);
} else {
    http_response_code(404);
    echo json_encode(array("message" => "Path not found"));
}
