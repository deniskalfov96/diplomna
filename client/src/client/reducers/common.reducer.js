import { SAVE_SCANNED_PLACE, REMOVE_SCANNED_PLACE, SET_PAGE_TITLE } from '../actions/types';

const initialState = {
    scanned_place: null,
    pagetitle: '',
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SAVE_SCANNED_PLACE:
            return {
                ...state,
                scanned_place: action.payload
            }
        case SET_PAGE_TITLE:
            return {
                ...state,
                pagetitle: action.payload
            }
        default:
            return state;
    }
}