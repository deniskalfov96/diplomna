import React, { Component } from 'react'
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { setPageTitle } from '../../client/actions/common.action';
import { getServices } from '../../client/actions/services.action';
// import { push } from 'react-router-redux';
import { ADMIN_URL } from '../../constants.js'
import { bindActionCreators } from 'redux'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import SideMenu from './SideMenu';


class ServicesAdminPage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setPageTitle('Services - управление - Администрация');
        this.props.getServices();
    }

    deleteTable = (e, id) => {
        e.preventDefault();
        // this.props.removeTable(id);
    }

    render() {
        let services = this.props.services;
        return (

            <Box sx={{ flexGrow: 1 }} style={{ maxWidth: '90%', margin: '0 auto', marginTop: 50 }}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>

                        <SideMenu
                            history={this.props.history} />


                    </Grid>
                    <Grid item xs={8}>

                        <TableContainer component={Paper}>
                            <Table aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell align="right">Place</TableCell>
                                        <TableCell align="right">Action</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>

                                    {services.map((row) => (
                                        <TableRow
                                            key={row.name}
                                        >
                                            <TableCell component="th" scope="row">
                                                {row.name}
                                            </TableCell>
                                            <TableCell align="right">{row.placeId}</TableCell>
                                            <TableCell align="right">
                                                <NavLink to={ADMIN_URL + 'services/edit/' + row.id} style={{ textDecoration: 'none' }}>
                                                    <Button variant="contained" >Edit</Button>
                                                </NavLink>

                                                &nbsp;

                                                <NavLink to={ADMIN_URL + 'services/edit/' + row.id} style={{ textDecoration: 'none' }}>
                                                    <Button variant="contained" color="error">Delete</Button>
                                                </NavLink>

                                                {/* <DeleteTableModal
                                        tableId={table.id}
                                        tableNum={table.table_num}
                                        deleteTable={deleteTable}
                                        translations={translations}
                                    /> */}

                                            </TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>

                    </Grid>
                </Grid>
            </Box>

        )
    }
}

const mapStateToProps = state => ({
    services: state.services.services,
});

const mapDispatchToProps = (dispatch, props) => {
    return {
        dispatch,
        ...bindActionCreators({ setPageTitle, getServices }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ServicesAdminPage);
