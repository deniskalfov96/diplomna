import axios from 'axios';
import { SAVE_SCANNED_PLACE, GET_SCANNED_PLACE, SET_PAGE_TITLE } from './types';
import { API_URL } from '../../constants'

export const saveScannedPlace = (data) => dispatch => {
    dispatch({
        type: SAVE_SCANNED_PLACE,
        payload: data
    })
}
export const setPageTitle = (value) => dispatch => {
    dispatch({
        type: SET_PAGE_TITLE,
        payload: value
    })
}
