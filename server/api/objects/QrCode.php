<?php

class QrCode
{
    private $conn;
    private $table_name = "qr_codes";

    public $id;
    public $token;
    public $active;
    public $place_id;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getAll()
    {
        $query = "SELECT q.id, q.token, q.active, q.place_id
        FROM {$this->table_name} as q
        ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
}