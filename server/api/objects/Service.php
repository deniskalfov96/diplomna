<?php

class Service
{
    private $conn;
    private $table_name = "services";

    public $id;
    public $name;
    public $date_created;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getAll()
    {
        $query = "SELECT s.id, s.name, s.placeId, 
        FROM {$this->table_name} as s
        ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function getServiceByPlace($placeId = 0)
    {
        $where = '';
        if ($placeId != 0) {
            $where .= " WHERE placeId = {$placeId} ";
        }
        
        $query = "SELECT s.id, s.name, s.placeId
        FROM {$this->table_name} as s
        {$where} 
        ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }



}