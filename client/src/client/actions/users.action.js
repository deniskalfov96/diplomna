import axios from 'axios';
import { GET_USERS } from './types';
import { API_URL } from '../../constants'

export const getUsers = () => dispatch => {
    // console.log('getting info')
    axios
        .get(API_URL + '?page=users/read')
        .then(res =>
            // console.log("services ", res.data),
            dispatch({
                type: GET_USERS,
                payload: res.data
            })
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}
