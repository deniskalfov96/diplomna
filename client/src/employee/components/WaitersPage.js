import React, { Component } from "react";
import { withStyles } from '@material-ui/styles';
import { connect } from "react-redux";
import { getAllWaitersForCurrentEmployee, updateWaiter } from "../actions/waiters.action.js";
import { setPageTitle } from '../../client/actions/common.action';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 700,
        backgroundColor: 'white',
        margin: '0 auto',
        marginTop: 20
    },
});

const statuses = {
    "completed": "Приключен",
    "canceled": "Отказан",
    "deleted": "Изтрит",
    "waiting": "Чакащ",
    "in_progress": "Обслужва се",
}


class WaitersPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            getWaiterInterval: '',
            currentTimeInterval: '',
            currentTime: (new Date()).getHours() + ':' + (new Date()).getMinutes(),
        }
    }

    componentDidMount() {
        this.props.setPageTitle('Чакащи клиенти');
        this.props.getAllWaitersForCurrentEmployee(localStorage.getItem('login_employee'));

        const self = this;
        let getWaiterInterval = setInterval(function () {
            self.props.getAllWaitersForCurrentEmployee(localStorage.getItem('login_employee'));
        }, 5000);
        this.setState({ getWaiterInterval });

        let currentTimeInterval = setInterval(function () {
            const now = new Date();
            self.setState({ currentTime: now.getHours() + ':' + now.getMinutes() })
        }, 1000);
        this.setState({ currentTimeInterval });

    }

    componentWillUnmount() {
        clearInterval(this.state.currentTimeInterval);
        clearInterval(this.state.getWaiterInterval);
    }

    getWaiterBackgroundColor = (status) => {
        switch (status) {
            case 'canceled':
                return 'red'
            case 'deleted':
                return 'black'
            case 'waiting':
                return 'initial'
            case 'in_progress':
                return 'yellow'
            case 'completed':
                return 'green'
            default:
                break;
        }
    }

    render() {
        const allWaitersForEmployee = this.props.all_waiters_for_employee;
        console.log(`allWaitersForEmployee`, allWaitersForEmployee)
        const { classes } = this.props;

        // const selectedService = this.props.services.find(s => s.id == waiter.service_id)

        return (
            <div className={classes.root}>
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        <List component="nav" aria-label="secondary mailbox folders">
                            <TextField id="outlined-basic" label="Гише номер" variant="outlined" name="gishe" defaultValue={localStorage.getItem('gishe')} onChange={e => localStorage.setItem('gishe', e.target.value)} style={{ width: '100%' }} />
                            <br />
                            <br />
                            {Object.values(allWaitersForEmployee).map(w => {
                                return (
                                    <>
                                        <ListItem
                                            // button
                                            key={w.id}
                                            style={{ backgroundColor: this.getWaiterBackgroundColor(w.status), borderBottom: '1px solid #ccc' }}
                                        // onClick={() => this.addWaiter(service.id)}
                                        >
                                            <ListItemText primary={<>Клиент #{w.number} - {statuses[w.status]}</>} />

                                            {w.status == 'waiting' ?
                                                <Button variant="outlined" style={{ width: '100%' }} onClick={(e) => this.props.updateWaiter({ id: w.id, status: 'in_progress', table_num: localStorage.getItem('gishe'), employee_id: JSON.parse(localStorage.getItem('login_employee')).id })} color="primary" style={{ width: 100 }}>Старт</Button>
                                                :
                                                null
                                            }
                                            {w.status == 'in_progress' ?
                                                <Button variant="outlined" style={{ width: '100%' }} onClick={(e) => this.props.updateWaiter({ id: w.id, status: 'completed' })} color="primary" style={{ width: 100 }}>Стоп</Button>
                                                :
                                                null
                                            }
                                        </ListItem>
                                    </>
                                );
                            })}
                        </List>
                    </Grid>
                    <Grid item xs={4}>
                        <Button variant="outlined" style={{ width: '100%' }} color="secondary"
                            style={{
                                marginTop: 15,
                                marginTop: 8,
                                padding: 10,
                                width: 119,
                                fontSize: 20,
                            }}
                        >{this.state.currentTime}</Button>
                    </Grid>
                </Grid>
            </div>
        );
    }
}

const mapStateToProops = (state, ownProps) => {
    return {
        all_waiters: state.e_waiter.all_waiters,
        all_waiters_for_employee: state.e_waiter.all_waiters_for_employee,
    };
};

export default (withStyles(styles)(connect(mapStateToProops, { getAllWaitersForCurrentEmployee, setPageTitle, updateWaiter })(WaitersPage)));
