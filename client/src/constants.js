let currentDomain = window.location.origin + '/';
let apiDomain = currentDomain;

// if(window.location.hostname == 'localhost') {
    // apiDomain = 'http://localhost:4000/'; // Custom api requests ulr //TODO
    apiDomain = 'https://denis.tabl.bg/diplomna_'; // Custom api requests ulr
// }

export const API_URL = apiDomain + "api/";
export const ADMIN_URL =  "/diplomna/admin/";

export const MAXIMUM_POSSIBLE_WAIT_TIME_HOURS = 5
export const CHOOSE_SERVICE_EXPIRE_TIME_HOURS = 1; // QR code will be valid until it is closed by employee (the user is serviced) or at least 1 hour (if he left) | (max - ~)
// this is used only for choosing service

// Image paths
export const SERVER_IMG_URL = apiDomain;
export const LOCAL_IMG_PATH = currentDomain;
