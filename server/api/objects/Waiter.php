<?php
error_reporting(0);

class Waiter
{
    private $conn;
    private $table_name = "waiters";

    public $id;
    public $number;
    public $service_id;
    public $status;
    public $created_at;
    public $completed_at;
    public $rating; // Still not used
    public $employee_id;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getAll()
    {
        $query = "SELECT w.id,
                w.number,
                w.status,
                w.employee_id,
                w.created_at,
                w.service_id,
                w.table_num
        FROM {$this->table_name} as w
        ORDER BY w.id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function getAllByWaiterId($waiterId)
    {
        $where = "";

        if ($waiterId && $waiterId>0) {
            $where = " WHERE w.id = ".$waiterId." ";
        }

        $query = "SELECT w.id,
                w.number,
                w.status,
                w.employee_id,
                w.created_at,
                w.service_id,
                w.table_num
        FROM {$this->table_name} as w
        {$where}
        ORDER BY w.id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function getAllByServiceIds($serviceIds)
    {

        $serviceIdsString = '';
        for ($i = 0; $i < sizeof($serviceIds);$i++) {
            $serviceIdsString.=$serviceIds[$i]['service_id'];
            if ($i !=sizeof($serviceIds)-1) {
                $serviceIdsString.=',';
            }
        }

        $query = "SELECT w.id,
                w.number,
                w.status,
                w.employee_id,
                w.created_at,
                w.service_id,
                w.table_num
        FROM {$this->table_name} as w
        WHERE w.service_id IN ({$serviceIdsString}) 
        ORDER BY w.id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;

    }


    function getUniqueDailyNumber()
    {
        $currentDate = date('Y-m-d');

        $query = "SELECT `number`
            FROM {$this->table_name}
            WHERE created_at between '{$currentDate} 00:00:00' and '{$currentDate} 23:59:59'
            ORDER BY id DESC 
            LIMIT 1
            ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC)['number'] + 1;
    }

    function getWaitingWaitersForSameService($serviceId)
    {
        $currentDate = date('Y-m-d');

        $query = "SELECT *
            FROM {$this->table_name}
            WHERE created_at between '{$currentDate} 00:00:00' and '{$currentDate} 23:59:59'
            AND `service_id` = ".$serviceId."
            AND `status` LIKE 'waiting'
            ORDER BY id ASC
            ";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }


    function create()
    {
        // query to insert record
        $query = "INSERT INTO {$this->table_name} SET number=:number, service_id=:service_id, created_at=:created_at ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->number = htmlspecialchars(strip_tags($this->number));
        $this->service_id = htmlspecialchars(strip_tags($this->service_id));
        $this->created_at = htmlspecialchars(strip_tags($this->created_at));

        // bind values
        $stmt->bindParam(":number", $this->number);
        $stmt->bindParam(":service_id", $this->service_id);
        $stmt->bindParam(":created_at", $this->created_at);

        // execute query
        if ($stmt->execute()) {
            return $this->conn->lastInsertId();
        }

        return false;
    }

    function update($id, $status = '', $rating = '', $employee_id = '', $table_num = '') {
        $updateSql = 'SET ';

        //var_dump($employee_id); die;//, $rating = '', $employee_id = '', $table_num = '')

        if ($status) { $updateSql .= " status = '{$status}' "; }
        if ($rating) { if ($updateSql != 'SET ') { $updateSql .= " , "; } $updateSql .= " rating = '{$rating}' "; }
        if ($employee_id) { if ($updateSql != 'SET ') { $updateSql .= " , "; } $updateSql .= " employee_id = '{$employee_id}' "; }
        if ($table_num) { if ($updateSql != 'SET ') { $updateSql .= " , "; } $updateSql .= " table_num = '{$table_num}' "; }

        $query = "UPDATE {$this->table_name} {$updateSql} WHERE id = '{$id}';";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // execute query
        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

    /*

            function increaseAmount($increasePercent, $increaseDate) {
                $increasePercent = floatval($increasePercent);
                $increaseDate = htmlspecialchars(strip_tags($increaseDate));

                $query = "START TRANSACTION;
                    UPDATE {$this->table_name} SET amount = amount + ({$increasePercent} * amount / 100) WHERE date_created LIKE '{$increaseDate}%';
                    COMMIT;";

                // prepare query
                $stmt = $this->conn->prepare($query);

                // execute query
                if ($stmt->execute()) {
                    return true;
                }

                return false;
            }
        */

}