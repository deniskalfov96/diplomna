import React, { Component } from 'react'
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { setPageTitle } from '../../client/actions/common.action';
import { getStatistics } from '../actions/statistics.actions';
// import { push } from 'react-router-redux';
import { ADMIN_URL } from '../../constants.js'
import { bindActionCreators } from 'redux'
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import SideMenu from './SideMenu';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class StatisticsAdminPage extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        this.props.setPageTitle('Статистика - Администрация');
        this.props.getStatistics();
    }

    deleteTable = (e, id) => {
        e.preventDefault();
        // this.props.removeTable(id);
    }

    render() {
        let statistics = this.props.statistics;

        let completedWaiters = 0;
        statistics.map(s => s.status == 'completed' ? completedWaiters++ : void (0));

        let inProgressWaiters = 0;
        statistics.map(s => s.status == 'in_progress' ? inProgressWaiters++ : void (0));

        return (

            <Box sx={{ flexGrow: 1 }} style={{ maxWidth: '90%', margin: '0 auto', marginTop: 50 }}>
                <Grid container spacing={2}>
                    <Grid item xs={4}>

                        <SideMenu
                            history={this.props.history} />


                    </Grid>
                    <Grid item xs={8}>

                        <Grid container spacing={2}>
                            <Grid item xs={4}>
                                <Card>
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div" style={{}}>
                                            Обслужени клиенти днес {completedWaiters}
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            Днес бяха обслужени <b>{completedWaiters} клиентa</b>, което е с <b>15% повече</b> от вчера.
                                            <br />
                                            <hr />
                                            <b>60%</b> от тях са оставили отзив 10/10, което е с <b>10% повече</b> от вчера.
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small">Вчера</Button>
                                        <Button size="small">Миналата седмица</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                            <Grid item xs={4}>
                                <Card>
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div" style={{}}>
                                            Средно време на обслужване
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            Средното време на обслужване днес <b>е 3.45 мин.</b>, което е с <b>17 сек. по-бързо</b> от вчера
                                        </Typography>
                                    </CardContent>
                                    <CardActions>
                                        <Button size="small">Вчера</Button>
                                        <Button size="small">Миналата седмица</Button>
                                    </CardActions>
                                </Card>
                            </Grid>
                            <Grid item xs={4}>
                                <Card>
                                    <CardContent>
                                        <Typography gutterBottom variant="h5" component="div" style={{}}>
                                            Обслужвани клиенти сега {inProgressWaiters}
                                        </Typography>
                                        <Typography variant="body2" color="text.secondary">
                                            В момента се обслужват <b>{inProgressWaiters} клиента</b>
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        </Grid>

                    </Grid>
                </Grid>
            </Box>

        )
    }
}

const mapStateToProps = state => ({
    statistics: state.statistics.statistics,
});

const mapDispatchToProps = (dispatch, props) => {
    return {
        dispatch,
        ...bindActionCreators({ setPageTitle, getStatistics }, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(StatisticsAdminPage);
