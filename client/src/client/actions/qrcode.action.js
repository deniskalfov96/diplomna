import axios from 'axios';
import { GET_QRCODES } from './types';
import { API_URL } from '../../constants'

export const getQRcodes = () => dispatch => {
    // console.log('getting info')
    axios
        .get(API_URL + '?page=qrcode/read')
        .then(res =>
            // console.log("services ", res.data),
            dispatch({
                type: GET_QRCODES,
                payload: res.data
            })
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}
