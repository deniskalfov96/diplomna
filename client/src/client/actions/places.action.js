import axios from 'axios';
import { GET_PLACES } from './types';
import { API_URL } from '../../constants'

export const getPlaces = () => dispatch => {
    // console.log('getting info')
    axios
        .get(API_URL + '?page=places/read')
        .then(res =>
            // console.log("services ", res.data),
            dispatch({
                type: GET_PLACES,
                payload: res.data
            })
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}
