import { combineReducers } from 'redux';
import statisticsReducer from './admin/reducers/statistics.reducer';
import commonReducer from './client/reducers/common.reducer';
import placesReducer from './client/reducers/places.reducer';
import qrcodeReducer from './client/reducers/qrcode.reducer';
import servicesReduces from './client/reducers/services.reducer';
import usersReducer from './client/reducers/users.reducer';
import waiterReducer from './client/reducers/waiter.reducer';
import waiterReducerEmployee from './employee/reducers/waiter.reducer';

export default combineReducers({
    services: servicesReduces,
    qrcodes: qrcodeReducer,
    places: placesReducer,
    common: commonReducer,
    e_waiter: waiterReducerEmployee,
    waiter: waiterReducer,
    users: usersReducer,
    statistics: statisticsReducer,
});