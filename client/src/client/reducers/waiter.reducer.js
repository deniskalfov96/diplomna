import { ADD_WAITER, GET_WAITER } from '../actions/types';

const initialState = {
}

export default function (state = initialState, action) {
    switch (action.type) {
        case ADD_WAITER:
            return {
                ...state,
                id: action.payload.id,
                number: action.payload.number,
                created_at: action.payload.created_at,
                service_id: action.payload.service_id,
                waitersSameService: action.payload.waitersSameService,
                employee_id: 0,
                status: action.payload.status,
                table_num: action.payload.table_num,
            }
        case GET_WAITER:
            return {
                ...state,
                id: action.payload.id,
                number: action.payload.number,
                created_at: action.payload.created_at,
                service_id: action.payload.service_id,
                waitersSameService: action.payload.waitersSameService,
                employee_id: action.payload.employee_id,
                status: action.payload.status,
                table_num: action.payload.table_num,
            }
        default:
            return state;
    }
}