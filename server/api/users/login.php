<?php

include_once ROOT.'objects/User.php';

$obj = new User($db);
$data = (object)$_POST;
$data->password = sha1($data->password);

$stmt = $obj->login($data);
$num = $stmt->rowCount();

if ($num > 0) {
    $obj_arr = array();
    $obj_arr = $stmt->fetch(PDO::FETCH_ASSOC);

    // set response code - 200 OK
    http_response_code(200);

    echo json_encode($obj_arr);
} else {
    // set response code - Not Found
    http_response_code(404);

    echo json_encode(
        array("message" => "No records")
    );
}
