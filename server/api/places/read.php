<?php
header("Access-Control-Request-Method: GET");

include_once ROOT.'objects/Place.php';

$obj = new Place($db);
$data = $_GET;

$stmt = $obj->getAll();
$num = $stmt->rowCount();

if ($num > 0) {
    $obj_arr = array();
    $obj_arr = array();

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
//        $row['bank_accounts'] = $obj->getBankAccountsForUser($row['id']);
        $obj_arr[] = $row;
    }

    // set response code - 200 OK
    http_response_code(200);

    echo json_encode($obj_arr);
} else {
    // set response code - Not Found
    http_response_code(404);

    echo json_encode(
        array("message" => "No records")
    );
}
