
import { GET_ALL_WAITERS, GET_ALL_WAITERS_FOR_EMPLOYEE } from '../actions/types';

const initialState = {
    all_waiters: [],
    all_waiters_for_employee: [],
}

export default function (state = initialState, action) {
    switch (action.type) {
        case GET_ALL_WAITERS:
            return {
                ...state,
                all_waiters: action.payload,
            }
        case GET_ALL_WAITERS_FOR_EMPLOYEE:
            return {
                ...state,
                all_waiters_for_employee: action.payload,
            }
        default:
            return state;
    }
}