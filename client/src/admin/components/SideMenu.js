import React, { Component } from 'react'
import { withStyles } from '@material-ui/styles';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { setPageTitle } from '../../client/actions/common.action';
import { getServices } from '../../client/actions/services.action';
// import { push } from 'react-router-redux';
import { ADMIN_URL } from '../../constants.js'
import { bindActionCreators } from 'redux'
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
// import Button from '@material-ui/core/Button';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import InboxIcon from '@material-ui/icons/MoveToInbox';
import DraftsIcon from '@material-ui/icons/Drafts';
import SendIcon from '@material-ui/icons/Send';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import StarBorder from '@material-ui/icons/StarBorder';
import Button from '@material-ui/core/Button';
import Equalizer from '@material-ui/icons/Equalizer';
import FeedbackIcon from '@material-ui/icons/Feedback';
import PeopleIcon from '@material-ui/icons/People';
import StorageIcon from '@material-ui/icons/Storage';
import QrCodeIcon from '@material-ui/icons/CropFree';
import PlaceIcon from '@material-ui/icons/Place';

const styles = theme => ({
    listItemButton: {
        display: 'block',
        width: '100%',
        '& span': {
            display: 'flex'
        }
    },
});

class SideMenu extends Component {

    render() {
        const { classes } = this.props
        return (

            <List
                sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }}
                component="nav"
                aria-labelledby="nested-list-subheader"
                subheader={
                    <ListSubheader component="div" id="nested-list-subheader">
                        Меню
                    </ListSubheader>
                }
            >
                <Button className={classes.listItemButton}>
                    <ListItemIcon>
                        <PlaceIcon />
                    </ListItemIcon>
                    <ListItemText primary="Обекти" />
                </Button>

                <Button className={classes.listItemButton}>
                    <ListItemIcon>
                        <QrCodeIcon />
                    </ListItemIcon>
                    <ListItemText primary="QR кодове" />
                </Button>

                <Button className={classes.listItemButton}
                    onClick={() => this.props.history.push(ADMIN_URL + 'services')}
                >
                    <ListItemIcon>
                        <StorageIcon />
                    </ListItemIcon>
                    <ListItemText primary="Услуги" />
                    {/* {open ? <ExpandLess /> : <ExpandMore />} */}
                </Button>


                <Button className={classes.listItemButton}
                // onClick={handleClick}
                >
                    <ListItemIcon>
                        <PeopleIcon />
                    </ListItemIcon>
                    <ListItemText primary="Служители" />
                    {/* {open ? <ExpandLess /> : <ExpandMore />} */}
                </Button>


                <Button className={classes.listItemButton}
                // onClick={handleClick}
                >
                    <ListItemIcon>
                        <FeedbackIcon />
                    </ListItemIcon>
                    <ListItemText primary="Отзиви" />
                    {/* {open ? <ExpandLess /> : <ExpandMore />} */}
                </Button>


                <Button className={classes.listItemButton}
                    onClick={() => this.props.history.push(ADMIN_URL + 'statistics')}
                >
                    <ListItemIcon>
                        <Equalizer />
                    </ListItemIcon>
                    <ListItemText primary="Статистика" />
                    {/* {open ? <ExpandLess /> : <ExpandMore />} */}
                </Button>

            </List>


        )
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
    };
};

export default (withStyles(styles)(connect(mapStateToProps, {})(SideMenu)));
