import React, { Component } from "react";
import { withStyles } from '@material-ui/styles';
import { connect } from "react-redux";
import { getServices } from "../actions/services.action";
import { addWaiter } from "../actions/waiters.action.js";
import { setPageTitle } from '../actions/common.action';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 500,
        backgroundColor: 'white',
        margin: '0 auto',
        marginTop: 20
    },
});

class ServicesPage extends Component {
    constructor(props) {
        super(props);
    }

    addWaiter = (serviceId) => {
        let formData = new FormData();    //formdata object
        formData.append('service_id', serviceId);   //append the values with key, value pair
        this.props.addWaiter(formData);
        this.props.history.push('/diplomna/waiter')
    }

    componentDidMount() {
        this.props.setPageTitle('Изберете услуга');
    }

    render() {
        const services = this.props.services;
        const { classes } = this.props;

        return (
            <React.Fragment>

                <div className={classes.root}>

                    <List component="nav" aria-label="secondary mailbox folders">
                        {Object.values(services).map(service => {
                            return (
                                <>
                                    <ListItem button
                                        key={service.id}
                                        onClick={() => this.addWaiter(service.id)}
                                    >
                                        <ListItemText primary={<>Услуга#{service.id} - {service.name}</>} />
                                    </ListItem>
                                </>
                            );
                        })}
                    </List>
                </div>

            </React.Fragment >
        );
    }
}

const mapStateToProops = (state, ownProps) => {
    return {
        services: state.services.services,
    };
};

export default (withStyles(styles)(connect(mapStateToProops, { getServices, addWaiter, setPageTitle })(ServicesPage)));

