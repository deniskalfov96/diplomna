import axios from 'axios';
import { API_URL } from '../../constants'

export const login = (obj) => {
    return axios
        .post(API_URL + '?page=users/login', obj)
}