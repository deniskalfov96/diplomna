import axios from 'axios';
import { GET_SERVICES } from './types';
import { API_URL } from '../../constants'

export const getServices = (placeId = 0) => dispatch => {
    // console.log('getting info')
    axios
        .get(API_URL + '?page=services/read&placeId=' + placeId)
        .then(res =>
            // console.log("services ", res.data),
            dispatch({
                type: GET_SERVICES,
                payload: res.data
            })
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}
