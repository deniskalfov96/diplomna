<?php

class Place
{
    private $conn;
    private $table_name = "places";

    public $id;
    public $name;
    public $photo;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getAll()
    {
        $query = "SELECT p.id, p.name, p.photo
        FROM {$this->table_name} as p
        ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }
}