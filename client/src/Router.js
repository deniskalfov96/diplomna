import React from 'react';
import { Switch, Route } from 'react-router-dom';

import ScanQrPage from './client/components/ScanQrPage';
import ServicesPage from './client/components/ServicesPage';
import WaiterPage from './client/components/WaiterPage';

import EmployeeLoginPage from './employee/components/LoginPage';
import EmployeeWaiterPage from './employee/components/WaitersPage';

import LoginAdminPage from './admin/components/LoginAdminPage'
import ServicesAdminPage from './admin/components/ServicesAdminPage';
import StatisticsAdminPage from './admin/components/StatisticsAdminPage';

const Router = () => (
    <div>
        <Switch>
            <Route exact path='/diplomna' component={ScanQrPage} />
            <Route exact path='/diplomna/services' component={ServicesPage} />
            <Route exact path='/diplomna/waiter' component={WaiterPage} /> {/* Here will be feedback form */}

            <Route exact path='/diplomna/employee/login' component={EmployeeLoginPage} />
            <Route exact path='/diplomna/employee/waiters' component={EmployeeWaiterPage} />

            <Route exact path='/diplomna/admin/login' component={LoginAdminPage} />
            {/* <Route exact path='/diplomna/admin/places' component={PlacesAdminPage} /> */}
            <Route exact path='/diplomna/admin/services' component={ServicesAdminPage} />
            {/* <Route exact path='/diplomna/admin/qrcodes' component={QrCodesAdminPage} /> */}
            {/* <Route exact path='/diplomna/admin/employees' component={EmployeesAdminPage} /> */}

            <Route exact path='/diplomna/admin/statistics' component={StatisticsAdminPage} />
            {/* <Route exact path='/diplomna/admin/feedback' component={FeedbackAdminPage} /> */}
        </Switch>
    </div>
)

export default Router;