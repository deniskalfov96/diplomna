<?php
error_reporting(0);

class Statistics
{
    private $conn;
    private $table_name = "waiters";

    public $id;
    public $number;
    public $service_id;
    public $status;
    public $created_at;
    public $completed_at;
    public $rating; // Still not used
    public $employee_id;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function getCompletedWaitersToday()
    {
        $currentDate = date('Y-m-d');

         $query = "SELECT *
            FROM {$this->table_name}
            WHERE created_at between '{$currentDate} 00:00:00' and '{$currentDate} 23:59:59'
            ORDER BY id ASC
            ";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

}