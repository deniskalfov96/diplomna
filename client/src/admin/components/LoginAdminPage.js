import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setPageTitle } from '../../client/actions/common.action';
import { login } from '../../employee/actions/login.action'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import axios from 'axios';
import { API_URL } from '../../constants'

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


class LoginAdminPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: '',
            password: '',
            role: 'admin',
            error: '',
        }
    }

    componentDidMount() {
        this.props.setPageTitle('Login - Администратори');
    }

    handleChange = (e) => {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit = (e) => {
        e.preventDefault();

        if (!!this.state.email && !!this.state.password) {
            axios
                .post(API_URL + '?page=users/login', this.state)
                .then(res => {
                    if (res && res.data && res.data.email) {
                        localStorage.setItem("login_admin", JSON.stringify(res.data))
                        setTimeout(() =>
                            this.props.history.push('/diplomna/admin/services')
                            , 300
                        )
                    } else {
                        this.setState({ error: 'Невалидни данни за вход' });
                    }
                })
                .catch(err => {
                    this.setState({ error: 'Невалидни данни за вход' });
                });
        } else {
            this.setState({ error: 'Моля попълнете и двете полета' });
        }
    }

    render() {
        const { email, password } = this.state;

        return (
            <>
                <Snackbar open={this.state.error.length > 0} autoHideDuration={6000} onClose={() => this.setState({ error: '' })}>
                    <Alert onClose={() => this.setState({ error: '' })} severity="error">
                        {this.state.error}
                    </Alert>
                </Snackbar>

                <div style={{ margin: '0 auto', marginTop: 20, width: 500 }}>
                    <form name="form" onSubmit={(e) => this.handleSubmit(e)}>
                        <div className={'form-group'}>
                            <TextField id="outlined-basic" label="Email" variant="outlined" name="email" value={email} onChange={e => this.handleChange(e)} style={{ width: '100%' }} />
                            <br />
                            <br />
                            <TextField id="outlined-basic" label="Password" variant="outlined" name="password" type="password" value={password} onChange={e => this.handleChange(e)} style={{ width: '100%' }} />
                        </div>
                        <div className="form-group">
                            <br />
                            <Button variant="outlined" style={{ width: '100%' }} onClick={(e) => this.handleSubmit(e)}>Login</Button>
                        </div>
                    </form>
                </div>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
    }
};
export default connect(mapStateToProps, { setPageTitle, login })(LoginAdminPage);