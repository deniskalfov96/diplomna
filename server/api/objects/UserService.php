<?php

class UserService
{
    private $conn;
    private $table_name = "user_services";

    public $id;
    public $service_id;
    public $employee_id;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    /*
     * Get users
     */


    function getServiceIdsByEmployeeId($employeeId)
    {
        $query = "SELECT DISTINCT us.service_id from {$this->table_name} as us WHERE us.employee_id = {$employeeId} ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

}