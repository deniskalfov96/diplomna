<?php
header("Access-Control-Allow-Origin: *");

include_once ROOT.'objects/Waiter.php';

$waiter = new Waiter($db);
$data = (object)$_POST;

$waiter->id = $data->id;
$waiter->status = $data->status;
$waiter->rating = $data->rating;
$waiter->employee_id = $data->employee_id;
$waiter->table_num= $data->table_num;

$editedWaiter = $waiter->update($waiter->id, $waiter->status, $waiter->rating, $waiter->employee_id, $waiter->table_num);

if ($editedWaiter) {
    http_response_code(200);
    echo
    json_encode(
        array(
            "id" => $createdWaiter,
            "number" => $waiter->number,
            "status" => $waiter->status,
            "employee_id" => $waiter->employee_id,
            "created_at" => $waiter->created_at,
            "service_id" => $waiter->service_id,
            "table_num" => $waiter->table_num,
            "waitersSameService" => ($waiter->getWaitingWaitersForSameService($waiter->service_id))->rowCount(),
        )
    );
} else {
    http_response_code(503);
    echo json_encode(array("error" => "Error occurred while editing record"));
}

?>