import axios from 'axios';
import { ADD_WAITER, GET_WAITER } from './types';
import { API_URL, MAXIMUM_POSSIBLE_WAIT_TIME_HOURS } from '../../constants'
import Cookies from 'universal-cookie';
const cookies = new Cookies();

export const addWaiter = (data) => dispatch => {
    axios
        .post(API_URL + '?page=waiters/add', data)
        .then((res) => {
            dispatch({
                type: ADD_WAITER,
                payload: res.data
            })
            // setWaiterNumber(res.data.number);
        }).catch(error => {
            console.log('erroring ', error);
            throw (error);
        });
}

// export const add = (data) => dispatch => {
//     axios
//         .post(API_URL + '?page=waiters/add', data)
//         .then((res) => {
//             dispatch({
//                 type: ADD_WAITER,
//                 payload: res.data
//             })
//             // setWaiterNumber(res.data.number);
//         }).catch(error => {
//             console.log('erroring ', error);
//             throw (error);
//         });
// }
export const getWaiter = (waiterId = 0) => dispatch => {
    axios
        .get(API_URL + '?page=waiters/read&waiterId=' + waiterId)
        .then(res => {
            dispatch({
                type: GET_WAITER,
                payload: waiterId > 0 ? (res.data && res.data.length == 1 && res.data[0] ? res.data[0] : {}) : res.data
            })
        }
        ).catch(error => {
            console.log('erroring ', error)
            throw (error);
        });
}


// const setWaiterNumber = (waiterNumber) => {
//     cookies.set('waiterNum', waiterNumber, { path: '/', expires: new Date(Date.now() + MAXIMUM_POSSIBLE_WAIT_TIME_HOURS * 60 * 60 * 1000) });
// }